<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "{$TMA_EU_COOKIE_BANNER_COLOR}",
      "text": "{$TMA_EU_COOKIE_BANNER_TEXT_COLOR}"
    },
    "button": {
      "background": "{$TMA_EU_COOKIE_BUTTON_COLOR}",
      "text": "{$TMA_EU_COOKIE_BUTTON_TEXT_COLOR}"
    }
  },
{if $TMA_EU_COOKIE_POSITION eq "bottom"}
{elseif $TMA_EU_COOKIE_POSITION eq "top"}
	 "position": "top",
{elseif $TMA_EU_COOKIE_POSITION eq "bottom-static"}
	"position": "top",
	"static": true,
{elseif $TMA_EU_COOKIE_POSITION eq "bottom-left"}
	"position": "bottom-left",
{elseif $TMA_EU_COOKIE_POSITION eq "bottom-right"}
	"position": "bottom-right",
{/if}
  "content": {
    "message": "{$TMA_EU_COOKIE_MESSAGE}",
    "dismiss": "{$TMA_EU_COOKIE_BUTTON_TEXT}",
    "link": "{$TMA_EU_COOKIE_LINK_TEXT}"
  }
})});
</script>