<?php

/*
 *  2017 Thorsten Marx
 * 
 *  @author Thorsten Marx <kontakt@thorstenmarx.com>
 *  @copyright  2016 Thorsten Marx
 *  @license    General Public License (GPLv3)
 */

if (!defined('_PS_VERSION_')) {
	exit;
}

class TMA_EU_Cookie extends Module {

	private $ps_layered_full_tree;
	private static $settings = array(
		"TMA_EU_COOKIE_POSITION",
		"TMA_EU_COOKIE_BANNER_COLOR",
		"TMA_EU_COOKIE_BANNER_TEXT_COLOR",
		"TMA_EU_COOKIE_BUTTON_COLOR",
		"TMA_EU_COOKIE_BUTTON_TEXT_COLOR",
		"TMA_EU_COOKIE_MESSAGE",
		"TMA_EU_COOKIE_BUTTON_TEXT",
		"TMA_EU_COOKIE_LINK_TEXT",
	);

	public function __construct() {
		$this->name = 'tma_eu_cookie';
		$this->tab = 'front_office_features';
		$this->version = '1.1.0';
		$this->author = 'Thorsten Marx';
		$this->need_instance = 0;
		$this->bootstrap = true;
		$this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => _PS_VERSION_);

		parent::__construct();

		$this->displayName = $this->l('TMA EU Cookie');
		$this->description = $this->l('EU Cookie for PrestaShop.');
	}

	public function install() {
		if (Shop::isFeatureActive()) {
			Shop::setContext(Shop::CONTEXT_ALL);
		}

		if (!parent::install() || !$this->registerHook('displayHeader')) {
			return false;
		}

		// set default values
		Configuration::updateValue('TMA_EU_COOKIE_POSITION', 'bottom');
		Configuration::updateValue('TMA_EU_COOKIE_BANNER_COLOR', '#000000');
		Configuration::updateValue('TMA_EU_COOKIE_BANNER_TEXT_COLOR', '#ffffff');
		Configuration::updateValue('TMA_EU_COOKIE_BUTTON_COLOR', '#f1d600');
		Configuration::updateValue('TMA_EU_COOKIE_BUTTON_TEXT_COLOR', '#000000');
		Configuration::updateValue('TMA_EU_COOKIE_MESSAGE', 'This website uses cookies to ensure you get the best experience on our website.');
		Configuration::updateValue('TMA_EU_COOKIE_BUTTON_TEXT', 'Got it!');
		Configuration::updateValue('TMA_EU_COOKIE_LINK_TEXT', 'Learn more');

		return true;
	}

	public function uninstall() {
		if (!parent::uninstall()) {
			return false;
		}


		return true;
	}

	public function hookDisplayHeader($params) {
		$this->context->controller->addCSS($this->_path . 'views/css/cookieconsent.min.css', 'all');
		$this->context->controller->addJS($this->_path . 'views/js/cookieconsent.min.js');

		$this->context->smarty->assign(array(
			"TMA_EU_COOKIE_POSITION" => Configuration::get('TMA_EU_COOKIE_POSITION'),
			"TMA_EU_COOKIE_BANNER_COLOR" => Configuration::get('TMA_EU_COOKIE_BANNER_COLOR'),
			"TMA_EU_COOKIE_BANNER_TEXT_COLOR" => Configuration::get('TMA_EU_COOKIE_BANNER_TEXT_COLOR'),
			"TMA_EU_COOKIE_BUTTON_COLOR" => Configuration::get('TMA_EU_COOKIE_BUTTON_COLOR'),
			"TMA_EU_COOKIE_BUTTON_TEXT_COLOR" => Configuration::get('TMA_EU_COOKIE_BUTTON_TEXT_COLOR'),
			"TMA_EU_COOKIE_MESSAGE" => Configuration::get('TMA_EU_COOKIE_MESSAGE'),
			"TMA_EU_COOKIE_BUTTON_TEXT" => Configuration::get('TMA_EU_COOKIE_BUTTON_TEXT'),
			"TMA_EU_COOKIE_LINK_TEXT" => Configuration::get('TMA_EU_COOKIE_LINK_TEXT'),
		));
		
		return $this->display(__FILE__, 'views/templates/hook/configuration.tpl');
	}

	public function getContent() {
		$output = null;

		foreach (TMA_EU_Cookie::$settings AS $name) {
			if (Tools::isSubmit('submit' . $this->name)) {
				$my_value = strval(Tools::getValue($name));
				if (!$my_value || empty($my_value) || !Validate::isGenericName($my_value))
					$output .= $this->displayError($this->l('Invalid Configuration value'));
				else {
					Configuration::updateValue($name, $my_value);
					$output .= $this->displayConfirmation($this->l('Settings updated: ' . $name));
				}
			}
		}

		return $output . $this->displayForm();
	}

	public function displayForm() {
		// Get default language
		$default_lang = (int) Configuration::get('PS_LANG_DEFAULT');

		$options_positions = array(
			array(
				'id_option' => 'bottom',
				'name' => $this->l('Banner bottom')
			),
			array(
				'id_option' => 'top',
				'name' => $this->l('Banner top')
			),
			array(
				'id_option' => 'bottom-static',
				'name' => $this->l('Banner top (pushdown)')
			),
			array(
				'id_option' => 'bottom-left',
				'name' => $this->l('Floating left')
			),
			array(
				'id_option' => 'bottom-right',
				'name' => $this->l('Floating right')
			),
		);

		// Init Fields form array
		$fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Settings'),
			),
			'input' => array(
				array(
					'type' => 'select',
					'label' => $this->l('Postion:'),
					'name' => 'TMA_EU_COOKIE_POSITION',
					'desc' => $this->l('The position, the cookie notification should be displayed.'),
					'required' => true,
					'options' => array(
						'query' => $options_positions,
						'id' => 'id_option',
						'name' => 'name'
					)
				),
				array(
					'type' => 'color',
					'label' => $this->l('Banner'),
					'name' => 'TMA_EU_COOKIE_BANNER_COLOR',
					'desc' => $this->l('The background color of the popup.'),
					'required' => true,
				), array(
					'type' => 'color',
					'label' => $this->l('Banner Text'),
					'name' => 'TMA_EU_COOKIE_BANNER_TEXT_COLOR',
					'desc' => $this->l('The text color of the popup text.'),
					'required' => true,
				),
				array(
					'type' => 'color',
					'label' => $this->l('Button'),
					'name' => 'TMA_EU_COOKIE_BUTTON_COLOR',
					'desc' => $this->l('The background color of the button.'),
					'required' => true,
				),
				array(
					'type' => 'color',
					'label' => $this->l('Button Text'),
					'name' => 'TMA_EU_COOKIE_BUTTON_TEXT_COLOR',
					'desc' => $this->l('The color of the button text.'),
					'required' => true,
				),
				array(
					'type' => 'textarea',
					'label' => $this->l('Message'),
					'name' => 'TMA_EU_COOKIE_MESSAGE',
					'desc' => $this->l('The popup message to display.'),
					'required' => true,
				),
				array(
					'type' => 'text',
					'label' => $this->l('Dismiss button text'),
					'name' => 'TMA_EU_COOKIE_BUTTON_TEXT',
					'size' => 20,
					'desc' => $this->l('The button text.'),
					'required' => true,
				),
				array(
					'type' => 'text',
					'label' => $this->l('Policy link text'),
					'name' => 'TMA_EU_COOKIE_LINK_TEXT',
					'size' => 20,
					'desc' => $this->l('The link text.'),
					'required' => true,
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'btn btn-default pull-right'
			)
		);

		$helper = new HelperForm();

		// Module, token and currentIndex
		$helper->module = $this;
		$helper->name_controller = $this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;

		// Language
		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;

		// Title and toolbar
		$helper->title = $this->displayName;
		$helper->show_toolbar = true;  // false -> remove toolbar
		$helper->toolbar_scroll = true;   // yes - > Toolbar is always visible on the top of the screen.
		$helper->submit_action = 'submit' . $this->name;
		$helper->toolbar_btn = array(
			'save' =>
			array(
				'desc' => $this->l('Save'),
				'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name .
				'&token=' . Tools::getAdminTokenLite('AdminModules'),
			),
			'back' => array(
				'href' => AdminController::$currentIndex . '&token=' . Tools::getAdminTokenLite('AdminModules'),
				'desc' => $this->l('Back to list')
			)
		);

		// Load current value
		$helper->fields_value['TMA_EU_COOKIE_POSITION'] = Configuration::get('TMA_EU_COOKIE_POSITION');
		$helper->fields_value['TMA_EU_COOKIE_BANNER_COLOR'] = Configuration::get('TMA_EU_COOKIE_BANNER_COLOR');
		$helper->fields_value['TMA_EU_COOKIE_BANNER_TEXT_COLOR'] = Configuration::get('TMA_EU_COOKIE_BANNER_TEXT_COLOR');
		$helper->fields_value['TMA_EU_COOKIE_BUTTON_COLOR'] = Configuration::get('TMA_EU_COOKIE_BUTTON_COLOR');
		$helper->fields_value['TMA_EU_COOKIE_BUTTON_TEXT_COLOR'] = Configuration::get('TMA_EU_COOKIE_BUTTON_TEXT_COLOR');
		$helper->fields_value['TMA_EU_COOKIE_BUTTON_TEXT'] = Configuration::get('TMA_EU_COOKIE_BUTTON_TEXT');
		$helper->fields_value['TMA_EU_COOKIE_LINK_TEXT'] = Configuration::get('TMA_EU_COOKIE_LINK_TEXT');
		$helper->fields_value['TMA_EU_COOKIE_MESSAGE'] = Configuration::get('TMA_EU_COOKIE_MESSAGE');

		return $helper->generateForm($fields_form);
	}

}
